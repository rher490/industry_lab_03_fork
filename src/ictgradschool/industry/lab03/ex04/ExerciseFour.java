package ictgradschool.industry.lab03.ex04;

/**
 * Write a program that prompts the user to enter an amount and a number of decimal places.  The program should then
 * truncate the amount to the user-specified number of decimal places using String methods.
 * <p>
 * <p>To truncate the amount to the user-specified number of decimal places, the String method indexOf() should be used
 * to find the position of the decimal point, and the method substring() should then be used to extract the amount to
 * the user-specified number of decimal places.  The program is to be written so that each task is in a separate method.
 * You need to write four methods, one method for each of the following tasks:</p>
 * <ul>
 * <li>Printing the prompt and reading the amount from the user</li>
 * <li>Printing the prompt and reading the number of decimal places from the user</li>
 * <li>Truncating the amount to the user-specified number of DP's</li>
 * <li>Printing the truncated amount</li>
 * </ul>
 */
public class ExerciseFour {

    private void start() {

        // TODO Use other methods you create to implement this program's functionality.
        String input = readTheInput();
        int n = findDecimalPlaces(input);
        String trunc = truncateNumber(input, n);
        printTruncatedAmount(trunc, n);
    }

    // TODO Write a method which prompts the user and reads the amount to truncate from the Keyboard
    private String readTheInput() {
        System.out.print("Please enter an amount: ");
        return ictgradschool.Keyboard.readInput();

    }

    // TODO Write a method which prompts the user and reads the number of DP's from the Keyboard
    private int findDecimalPlaces(String input) {
        System.out.print("Please enter the number of decimal places: ");
        String s = ictgradschool.Keyboard.readInput();
        return Integer.parseInt(s);
    }

    // TODO Write a method which truncates the specified number to the specified number of DP's
    private String truncateNumber(String s, int n) {
        int indexOfDecimal = s.indexOf('.');
//        System.out.println("s = [" + s + "], n = [" + n + "]");
//        System.out.println("decimal = " + decimal);
//        System.out.println(decimal + n);

        //in the substring
        return s.substring(0, indexOfDecimal + 1 + n);

    }

    // TODO Write a method which prints the truncated amount
    private void printTruncatedAmount(String trunc, int n) {
        System.out.println("Amount truncated to " + n + " decimal places is: " + trunc);
//        System.out.println("01234567".substring(0,4));
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        ExerciseFour ex = new ExerciseFour();
        ex.start();
    }
}
