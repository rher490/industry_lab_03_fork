package ictgradschool.industry.lab03.ex02;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class ExerciseTwo {

    /**
     * TODO Your code here. You may also write additional methods if you like.
     */
    private void start() {
        evaluateOfExpressions();

    }

    private void evaluateOfExpressions() {
        System.out.print("Lower bound? ");
        int lowerBound = Integer.parseInt(ictgradschool.Keyboard.readInput());
        System.out.print("Upper bound? ");
        int upperBound = Integer.parseInt(ictgradschool.Keyboard.readInput());

//        if(lowerBound>upperBound){
//            System.out.println("Lower bound is higher than upper bound");
//        }

        int a = (int) (Math.random() * (upperBound - lowerBound + 1)) + lowerBound;
        int b = (int) (Math.random() * (upperBound - lowerBound + 1)) + lowerBound;
        int c = (int) (Math.random() * (upperBound - lowerBound)) + lowerBound;
        System.out.println("3 randomly generated numbers: " + a + " " + b + " and " + c);
        int min = Math.min(a, Math.min(b, c));
        System.out.println("Smallest number is " + min);
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();

    }
}
